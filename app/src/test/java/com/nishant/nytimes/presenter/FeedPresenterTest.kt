package com.nishant.nytimes.presenter

import android.content.Context
import com.android.volley.VolleyError
import com.nishant.nytimes.FeedContract
import com.nishant.nytimes.presenter.interactor.FeedInteractor
import org.junit.Test

import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations


class FeedPresenterTest {


    @Mock
    lateinit var interactor: FeedInteractor

    @Mock
    lateinit var view: FeedContract.IView

    @Mock
    lateinit var context: Context

    private lateinit var presenter: FeedContract.IPresenter


    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = FeedPresenter(view, interactor)
    }

    @Test
    fun detachView() {

    }


    @Test
    fun requestApi() {
        presenter.requestApi(context)
        verify(interactor).getFeeds(presenter)
    }

    @Test
    fun onFeedFailed() {
        val v = VolleyError()
        presenter.onFeedFailed(v)
        verify(view).onError(v)
    }

    @Test
    fun onFeedReceived() {
        val res = ""
        presenter.onFeedReceived(res)
        verify(view).onAPIResultSuccess(any())
    }
}