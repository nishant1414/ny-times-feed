package com.nishant.nytimes.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.android.volley.toolbox.NetworkImageView
import com.nishant.nytimes.R
import com.nishant.nytimes.model.ResultsItem
import com.nishant.nytimes.network.VolleySingletonWrapper


class FeedAdapter(private val itemList: List<ResultsItem?>, private val context: Context, private val listener: RecyclerViewClickListener) : RecyclerView.Adapter<FeedAdapter.ViewHolder>() {


    interface RecyclerViewClickListener {
        fun onClick(view: View, position: Int)
    }

    inner class ViewHolder(private val view: View, private val listener: RecyclerViewClickListener) :
        RecyclerView.ViewHolder(view), View.OnClickListener {
        override fun onClick(p0: View?) {
            listener.onClick(view, adapterPosition)
        }

        init {
            view.setOnClickListener(this)
        }

        val titleTextView: TextView = view.findViewById(R.id.titleTextView)
        val authorTextView: TextView = view.findViewById(R.id.authorTextView)
        val dateTextView: TextView = view.findViewById(R.id.dateTextView)
        val thumbnailImage: NetworkImageView = view.findViewById(R.id.thumbnailImage)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_view_item_layout, parent, false)
        return ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        itemList[position].let { item ->
            holder.titleTextView.text = item?.title
            holder.authorTextView.text = item?.byline
            holder.dateTextView.text = item?.publishedDate
            holder.thumbnailImage.setErrorImageResId(R.mipmap.ic_launcher)
            holder.thumbnailImage.setImageUrl(
                item?.media?.get(0)?.mediaMetadata?.get(0)?.url,
                VolleySingletonWrapper.getImageLoader(context)
            )
        }
    }


    override fun getItemCount() = itemList.size
}