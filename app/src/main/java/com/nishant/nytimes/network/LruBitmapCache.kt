package com.nishant.nytimes.network

import android.graphics.Bitmap
import android.support.v4.util.LruCache
import com.android.volley.toolbox.ImageLoader

class LruBitmapCache(maxSize: Int) : LruCache<String, Bitmap>(maxSize), ImageLoader.ImageCache {

    @Synchronized
    override fun sizeOf(key: String, value: Bitmap): Int {
        return value.byteCount / 1024
    }

    @Synchronized
    override fun getBitmap(url: String): Bitmap? {
        return get(url)
    }

    @Synchronized
    override fun putBitmap(url: String, bitmap: Bitmap) {
        put(url, bitmap)
    }

}