package com.nishant.nytimes.network

import android.content.Context
import com.nishant.nytimes.Const
import com.nishant.nytimes.R

class UrlManager {
    fun getFeedUrl(context: Context, period:String):String{
        return context.getString(R.string.viewed_url, period, Const.NYT_KEY)
    }
}