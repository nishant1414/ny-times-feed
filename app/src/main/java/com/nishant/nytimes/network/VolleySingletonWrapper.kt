package com.nishant.nytimes.network

import android.content.Context
import android.graphics.Bitmap
import com.android.volley.RequestQueue
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.ImageLoader


object VolleySingletonWrapper{

    private var requestQueue: RequestQueue? = null
    private const val imageCacheSize = 5 * 1024 * 1024 // 5MiB

    fun getRequestQueue(context: Context): RequestQueue {
        if (requestQueue == null) {
            val network = BasicNetwork(HurlStack())
            //caching enabled by default(can be disabled using false)
            val responseCacheSize = DiskBasedCache(context.cacheDir, 2 * 1024 * 1024) // 2MiB

            val cachePolicy: com.android.volley.Cache = responseCacheSize
            requestQueue = RequestQueue(cachePolicy, network)
            // Start the queue
            requestQueue!!.start()
        }
        return requestQueue as RequestQueue
    }

    fun getImageLoader(context: Context): ImageLoader {
        return ImageLoader(getRequestQueue(context), object : ImageLoader.ImageCache {
            private val cache = LruBitmapCache(imageCacheSize)
            override fun getBitmap(url: String): Bitmap? {
                return cache.get(url)
            }

            override fun putBitmap(url: String, bitmap: Bitmap) {
                cache.put(url, bitmap)
            }
        })
    }
}