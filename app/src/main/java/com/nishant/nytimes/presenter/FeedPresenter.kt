package com.nishant.nytimes.presenter

import android.content.Context
import com.android.volley.VolleyError
import com.google.gson.GsonBuilder
import com.nishant.nytimes.FeedContract
import com.nishant.nytimes.presenter.interactor.FeedInteractor
import com.nishant.nytimes.model.FeedResponse

class FeedPresenter(private var view: FeedContract.IView?, private val interactor:FeedInteractor) : FeedContract.IPresenter {

    override fun detachView() {
        view = null
    }

    override fun onLoad(context: Context) {
        requestApi(context)
        view?.showProgressBarDialog(false)
    }

    override fun requestApi(context: Context) {
        interactor.getFeeds(this)
    }

    override fun onFeedFailed(volleyError: VolleyError) {
        view?.onError(volleyError)
        view?.hideProgressBarDialog()
    }

    override fun onFeedReceived(response: String) {
        view?.onAPIResultSuccess(GsonBuilder().create().fromJson(response, FeedResponse::class.java))
        view?.hideProgressBarDialog()
    }

}