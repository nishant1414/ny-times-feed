package com.nishant.nytimes.presenter

import android.content.Context
import com.nishant.nytimes.ArticleContract

class ArticlePresenter(private var view:ArticleContract.IView?):ArticleContract.IPresenter {
    override fun onExtrasFetch() {
        view?.setDataToUI()
    }

    override fun onLoad(context: Context) {
        view?.getExtras()
    }

    override fun detachView() {
        view = null
    }
}