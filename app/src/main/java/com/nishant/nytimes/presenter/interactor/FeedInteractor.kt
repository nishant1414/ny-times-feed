package com.nishant.nytimes.presenter.interactor

import android.content.Context
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.nishant.nytimes.FeedContract
import com.nishant.nytimes.network.UrlManager
import com.nishant.nytimes.network.VolleySingletonWrapper

interface InteratorInput{
    fun getFeeds(presenter: FeedContract.IPresenter)
}

class FeedInteractor(val context: Context):
    InteratorInput {

    override fun getFeeds(presenter: FeedContract.IPresenter){
        val queue = VolleySingletonWrapper.getRequestQueue(context)
        val url = UrlManager().getFeedUrl(context=context, period = "7")

        val stringRequest = StringRequest(
            Request.Method.GET, url,
            Response.Listener<String> { response ->
              presenter.onFeedReceived(response)
            },
            Response.ErrorListener {
              presenter.onFeedFailed(it)
            })
        queue.add(stringRequest)
    }
}