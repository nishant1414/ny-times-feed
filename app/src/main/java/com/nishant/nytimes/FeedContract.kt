package com.nishant.nytimes

import android.content.Context
import com.android.volley.VolleyError
import com.nishant.nytimes.base.BasePresenter
import com.nishant.nytimes.base.BaseView
import com.nishant.nytimes.model.FeedResponse

class FeedContract {

    interface IView:BaseView {
        fun onError(volleyError: VolleyError?)
        fun onAPIResultSuccess(response: FeedResponse?)
        fun showProgressBarDialog(isCancelable: Boolean)
        fun hideProgressBarDialog()
    }

    interface IPresenter:BasePresenter {
        fun requestApi(context: Context)
        fun onFeedReceived(response: String)
        fun onFeedFailed(volleyError: VolleyError)


    }

}