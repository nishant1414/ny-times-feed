package com.nishant.nytimes.base

import android.content.Context

interface BaseView {
    fun attachPresenter()
    fun detachPresenter()
}


interface BasePresenter {
    fun onLoad(context: Context)
    fun detachView()
}
