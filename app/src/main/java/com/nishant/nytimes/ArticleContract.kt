package com.nishant.nytimes

import com.nishant.nytimes.base.BasePresenter
import com.nishant.nytimes.base.BaseView

class ArticleContract {

    interface IView :BaseView{
        fun getExtras()
        fun setDataToUI()
    }

    interface IPresenter :BasePresenter{
        fun onExtrasFetch()
    }

}