package com.nishant.nytimes.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.android.volley.VolleyError
import com.nishant.nytimes.FeedContract
import com.nishant.nytimes.adapter.FeedAdapter
import com.nishant.nytimes.R
import com.nishant.nytimes.presenter.FeedPresenter
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent
import com.nishant.nytimes.model.FeedResponse
import com.nishant.nytimes.presenter.interactor.FeedInteractor


class MainActivity : AppCompatActivity(), FeedContract.IView, FeedAdapter.RecyclerViewClickListener {



    private var mPresenter: FeedPresenter? = null
    private var mProgressBarDialog: AlertDialog? = null
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var mAdapter: FeedAdapter? = null
    private var mResponse: FeedResponse? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        attachPresenter()
    }

    /**
     * attaches the presenter to the view
     */
    override fun attachPresenter() {
        mPresenter = FeedPresenter(this, FeedInteractor(this))
        mPresenter?.onLoad(this)
    }

    /**
     * receives a callback on api failure
     */
    override fun onError(volleyError: VolleyError?) {
        //TODO: Handle failure
    }

    /**
     * receives a callback on api success
     */
    override fun onAPIResultSuccess(response: FeedResponse?) {
        response?.let {
            mResponse = it
            setListItems(it)
        }
    }

    /**
     * set response items to adapter to show on recyclerView
     */
    private fun setListItems(response: FeedResponse) {
        response.results?.let {
            mAdapter = FeedAdapter(it, this, this)
            viewManager = LinearLayoutManager(this)
            recyclerView.apply {
                layoutManager = viewManager
                adapter = mAdapter
            }
        }
    }

    /**
     * creates progressbar dialog
     */

    private fun createProgressDialog(isCancelable: Boolean): AlertDialog {
        val inflater = layoutInflater
        val nullParent: ViewGroup? = null
        val alertLayout = inflater.inflate(R.layout.custom_progress_dialog_layout, nullParent)
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(isCancelable) // if you want user to wait for some process to finish,
        builder.setView(alertLayout)
        return builder.create()
    }

    /**
     * shows progressbar (initialize if null)
     */
    override fun showProgressBarDialog(isCancelable: Boolean) {
        if (mProgressBarDialog == null) {
            mProgressBarDialog = createProgressDialog(isCancelable)
            mProgressBarDialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        }
        mProgressBarDialog!!.show()
    }

    /**
     * hides progressbar if shown and not null
     */
    override fun hideProgressBarDialog() {
        mProgressBarDialog?.dismiss()
    }

    override fun onClick(view: View, position: Int) {

        val intent = Intent(this, ArticleDetailsActivity::class.java)
        val bundle = Bundle()
        mResponse?.results?.let {
            bundle.putSerializable(ArticleDetailsActivity.ARTICLE, it[position])
        }
        intent.putExtras(bundle)
        startActivity(intent)

    }

    override fun detachPresenter() {
        mPresenter?.detachView()
        mPresenter = null
    }

    override fun onDestroy() {
        super.onDestroy()
        detachPresenter()
    }
}
