package com.nishant.nytimes.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.nishant.nytimes.ArticleContract
import com.nishant.nytimes.R
import com.nishant.nytimes.model.ResultsItem
import com.nishant.nytimes.presenter.ArticlePresenter
import kotlinx.android.synthetic.main.activity_article_details.*
import android.view.MenuItem


class ArticleDetailsActivity : AppCompatActivity(), ArticleContract.IView {

    private var mPresenter: ArticlePresenter? = null

    companion object {
        const val ARTICLE = "article"
    }

    private var article: ResultsItem? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_details)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        attachPresenter()
    }

    override fun getExtras() {
        val extras = intent.extras
        if (extras != null) {
            article = intent.getSerializableExtra(ARTICLE) as ResultsItem
        }
        mPresenter?.onExtrasFetch()
    }

    override fun setDataToUI() {
        titleTextView.text = article?.title
        abstractTextView.text = article?.title
    }

    override fun attachPresenter() {
        mPresenter = ArticlePresenter(this)
        mPresenter?.onLoad(this)
    }

    override fun detachPresenter() {
        mPresenter?.detachView()
    }

    override fun onDestroy() {
        detachPresenter()
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}

