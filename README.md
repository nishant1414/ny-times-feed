# NY Times

This project is build on kotlin 1.3.21, Its architecture is build on MVP. It is recommended that you use latest version of android studio.

To improt this project you can clone it from repository and can import in android studio.

Some of the methods of presenter are covered in Unit test cases to show example of testibility. In this project presenter does not have any calculation or processing on inputs so only callbacks are covered in tests.

Jacoco is integrated to show test results and test coverage. Jacoco can be run from the gradle task (can be seen withing right top corner gradle tab in studio)or can be excuted from commad line. To run from commonad line you can run ./gradlew createDebugCoverageReport. Generated report can be found at /build/outputs/reports/coverage/debug/

Lint report can be found at /build/outputs/reports

APK can be found at root of the project.

Created By - Nishant Kumar er.nishant.sharma.cs@gmail.com